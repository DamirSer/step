from django.db import models
import datetime
from user_management.models import Teacher, SimpleUser


# Create your models here.


class Course(models.Model):
    teacher = models.ForeignKey(Teacher)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=500)
    start_date = models.DateField
    end_date = models.DateField


class Lesson(models.Model):
    name = models.CharField(max_length=40)
    course = models.ForeignKey(Course)
    number = models.PositiveSmallIntegerField
    description = models.TextField(max_length=500)


class MaterialPage(models.Model):
    name = models.CharField(max_length=40)
    lesson = models.ForeignKey(Lesson)
    text = models.TextField(max_length=1000)

class Comment(models.Model):
    text = models.TextField(max_length=500)
    user = models.ForeignKey(SimpleUser)
    task = models.ForeignKey(MaterialPage)
    pub_date = models.DateTimeField(default=datetime.datetime.today)
    question = models.BooleanField(default=False)