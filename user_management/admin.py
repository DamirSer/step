from django.contrib import admin

from user_management.models import *

admin.site.register(SimpleUser)
admin.site.register(Company)
admin.site.register(Teacher)
