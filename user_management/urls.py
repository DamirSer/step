from django.conf.urls import url
from django.contrib.auth import views as auth_views
from user_management.views import *

urlpatterns = [
    url(r'modify$', modify_profile, name='modify'),
    url(r'change_password', change_password, name='change_password'),
    url(r'^login$', auth_views.login, {'template_name': 'user_management/login.html'}, name='login'),
    url(r'^logout$', auth_views.logout, {'next_page': '/login'}, name='logout'),
    url(r'^registration/$', student_register, name="student_registration"),
    url(r'^teacher_adding/$', teacher_adding, name="teacher_adding"),
    url(r'^company_registration/$', company_register, name="company_registration"),
    url(r'^profile/', profile, name="profile"),
]
