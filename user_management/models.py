from django.db import models
from django.contrib.auth.models import User


class Company(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=40)
    is_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class SimpleUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    second_name = models.CharField(max_length=40)
    third_name = models.CharField(max_length=40)
    banned = models.BooleanField(default=False)
    def create_user(self, first_name, second_name, third_name, email, password):
        user = User(first_name=first_name, username=email, password=password)
        user.save()
        user = SimpleUser(user=user, second_name=second_name, third_name=third_name)
        user.save()
        return user
    def get_first_name(self):
        return self.user.first_name
    def get_second_name(self):
        return self.second_name
    def get_third_name(self):
        return self.third_name
    def get_email(self):
        return self.user.username
    def get_password(self):
        return self.user.password
    def set_first_name(self, first_name):
        self.user.first_name = first_name
        self.user.save()
        self.save()
    def set_second_name(self, second_name):
        self.second_name = second_name
        self.save()
    def set_third_name(self, third_name):
        self.third_name = third_name
        self.save()
    def set_email(self, email):
        self.user.username = email
        self.user.save()
        self.save()
    def set_password(self, password):
        self.user.set_password(password)
        self.user.save()
        self.save()

    def __str__(self):
        return self.user.username + ', ' + \
               ' '.join([self.user.first_name, self.second_name, self.third_name])

    def is_teacher(self):
        return hasattr(self, 'teacher')

    def is_student(self):
        return not self.is_teacher()


class Teacher(models.Model):
    user = models.OneToOneField(SimpleUser, on_delete=models.CASCADE)
    description = models.TextField(max_length=500)
    company = models.ForeignKey(Company)

    def __str__(self):
        return self.use

class Admin(models.Model):
    user = models.ForeignKey(SimpleUser)
