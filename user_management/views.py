from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as auth_login
from django.contrib.auth.hashers import make_password, check_password
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.contrib import messages

from user_management.helpers import validator
from user_management.models import *
from user_management.models import SimpleUser


# Create your views here.

@login_required
def modify_profile(request):
    if request.method == 'GET':
        user = SimpleUser.objects.get(user=request.user)
        first_name = user.get_first_name()
        second_name = user.get_second_name()
        third_name = user.get_third_name()
        email = user.get_email()
        return render(request, 'user_management/modify_profile.html', {'first_name': first_name,
                                                                       'second_name': second_name,
                                                                       'third_name': third_name,
                                                                       'email': email})
    if request.method == 'POST':
        first_name = request.POST['first_name']
        second_name = request.POST['second_name']
        third_name = request.POST['third_name']
        email = request.POST['email']
        errors = ''
        if not validator.is_fields_filled(first_name, second_name, third_name, email):
            errors = 'Fill all fields'
        elif not validator.is_correct_email(email):
            errors = 'Incorrect email'
        elif not validator.is_correct_name(first_name):
            errors = 'Incorrect name'
        elif not validator.is_correct_name(second_name):
            errors = 'Incorrect second name'
        elif not validator.is_correct_name(third_name):
            errors = 'Incorrect third name'
        elif not email == request.user.username and not validator.is_email_free(email):
            errors = 'Email is already taken'
        else:
            user = SimpleUser.objects.get(user=request.user)
            user.set_first_name(first_name)
            user.set_second_name(second_name)
            user.set_third_name(third_name)
            user.set_email(email)
        messages.add_message(request, messages.INFO, errors)
        return HttpResponseRedirect(reverse('user_management:modify'))
    else:
        return HttpResponse('Not supported')


@login_required
def change_password(request):
    if request.method != 'POST':
        return HttpResponse('Not supported')
    else:
        errors = ''
        user = SimpleUser.objects.get(user=request.user)
        if not check_password(request.POST['old_password'], user.get_password()):
            errors = 'Wrong old password'
        elif request.POST['new_password'] == '' or request.POST['repeated_password'] == '':
            errors = 'Fill all fields'
        elif request.POST['new_password'] != request.POST['repeated_password']:
            errors = 'Wrong repeated password'
        elif not validator.is_correct_password(request.POST['new_password']):
            errors = 'Пароль должен содержать от шести до тридцати симовлов, может содержать буквы латиницы, цифры, -, _'
        else:
            user.set_password(request.POST['new_password'])
        messages.add_message(request, messages.INFO, errors)
        user = authenticate(username= user.get_email(), password=request.POST['new_password'])
        auth_login(request, user)
        return HttpResponseRedirect(reverse('user_management:modify'))

def student_register(request):
    if request.method == "POST":
        result = simple_user_registration(request)
        if isinstance(result, SimpleUser):
            return HttpResponseRedirect(reverse('user_management:login'))
        else:
            return render(request, "user_management/registration.html", result)

    else:
        return render(request, "user_management/registration.html")


def company_register(request):
    if request.method == "POST":
        email = request.POST['email']
        company_name = request.POST['company_name']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']

        args = {'email': email, 'company_name': company_name}

        if not validator.is_fields_filled(email, company_name, password, confirm_password):
            args['error'] = 'Please fill in all fields'
        elif not validator.is_correct_email(email):
            args['error'] = 'Invalid mail format'
        elif not validator.is_email_free(email):
            args['error'] = 'User with this mail already exists'
        elif not validator.is_correct_password(password):
            args['error'] = 'Invalid password format'
        elif not validator.is_passwords_match(password, confirm_password):
            args['error'] = 'Passwords don\'t match'

        if args.get('error'):
            return render(request, "user_management/company_registration.html", args)

        user = User(username=email,
                    password=make_password(password, salt=None, hasher='default'))
        user.save()

        company = Company(user=user, name=company_name)
        company.save()
        return HttpResponse("Your application for registration has been sent to the administrator for confirmation")

    else:
        return render(request, "user_management/company_registration.html")


@login_required
def teacher_adding(request):
    user = request.user
    if not hasattr(user, 'company') or not user.company.is_confirmed:
        raise Http404

    if request.method == "POST":
        description = request.POST['description']

        result = simple_user_registration(request)
        if isinstance(result, SimpleUser):
            teacher = Teacher(user=result, description=description, company=request.user.company)
            teacher.save()
            return HttpResponse('The teacher is registered. Here will be move to the list of teachers page')
        else:
            result['description'] = description
            return render(request, "user_management/teacher_adding.html", result)

    else:
        return render(request, "user_management/teacher_adding.html")


def simple_user_registration(request):
    email = request.POST['email']
    first_name = request.POST['first-name']
    second_name = request.POST['second-name']
    third_name = request.POST['third-name']
    password = request.POST['password']
    confirm_password = request.POST['confirm_password']

    args = {'email': email, 'first_name': first_name, 'second_name': second_name, 'third_name': third_name}

    if not validator.is_fields_filled(email, first_name, second_name, third_name, password, confirm_password):
        args['error'] = 'Please fill in all fields'
    elif not validator.is_correct_email(email):
        args['error'] = 'Invalid mail format'
    elif not validator.is_email_free(email):
        args['error'] = 'User with this mail already exists'
    elif not validator.is_correct_password(password):
        args['error'] = 'Invalid password format'
    elif not validator.is_passwords_match(password, confirm_password):
        args['error'] = 'Passwords don\'t match'
    elif not validator.is_correct_name(first_name, second_name, third_name):
        args['error'] = 'Invalid name format'

    if args.get('error'):
        return args

    user = User(username=email,
                password=make_password(password, salt=None, hasher='default'),
                first_name=first_name)
    user.save()
    simple_user = SimpleUser(user=user, second_name=second_name, third_name=third_name)
    simple_user.save()
    return simple_user


def profile(request):
    return render(request, "user_management/profile.html")
